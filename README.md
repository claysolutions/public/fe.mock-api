# Mock API

Creates a mock API server based on contracts (json-schemas) that allows development of features without waiting for BE to implement the endpoints.
Powered by [json-server](https://github.com/typicode/json-server), [json-schema-faker](https://github.com/json-schema-faker/json-schema-faker) and [faker.js](https://github.com/marak/Faker.js/).

## Installation

1. Install using `npm i -D fe.mock-api`
2. Create `json-server.json` at the root of the project with the desired options.
3. Make sure you allow requests to `localhost` in your CSP.

```
{
  "schemaPath": "schema",
  "mainSchema": "index.json"
}
```

## Options

| Option         | Default          | Description                                                    |
| -------------- | ---------------- | -------------------------------------------------------------- |
| `schemaPath`\* | `none`           | Path to the folder where schemas are stored                    |
| `mainSchema`\* | `none`           | Main schema file name                                          |
| `port`         | `4000`           | Port where the server will be attached                         |
| `pageSize`     | `20`             | Page size for pagination                                       |
| `delay`        | `0`              | Delay time for the response in ms                              |
| `dbPath`       | `none`           | Path relative to the root where the generated db will be saved |
| `items`        | `items`          | Name for the `items` property                                  |
| `count`        | `count`          | Name for the `count` property                                  |
| `nextPageLink` | `next_page_link` | Name for the `next_page_link` property                         |

## Usage

- Run `fe.mock-api` to launch the server. The settings will be loaded from the `json-server.json` file.
- The endpoints will be automatically generated based on the top-level properties of the database.
- Every endpoint (where appropriate) supports `GET`, `POST`, `PUT`, `PATCH` and `DELETE`.
- The folder containing schemas as defined by `schemaPath` must contain only `.json` schema files.

## Error endpoints

Testing errors is possible by either targeting a specific route with the corresponding error code or by appending the `_error` query param at the end of any request. A response body will be returned if there is a matching property in the database with the specified error code.

| Example                           | Response code |
| --------------------------------- | ------------- |
| `localhost:4000/501`              | `501`         |
| `localhost:4000/users?_error=404` | `404`         |

### Custom response body:

`schema.json`

```
{
  "id": "404",
  "type": "object",
  "required": ["errorMessage", "errorCode"],
  "properties": {
    "errorMessage": {
      "type": "string",
      "faker": "lorem.sentence"
    },
    "errorCode": {
      "type": "integer",
      "minimum": "1"
    }
  }
}
```

`localhost:4000/404` response body

```
{
  "errorMessage": "Voluptatibus id dolorum officia voluptatem cupiditate.",
  "errorCode": 33182152
}
```

## Helper methods

To correctly construct the URL of a request the method `getMockUrl` is exposed.

```
import { getMockUrl } from 'fe.mock-api';

const endpoint = getMockUrl({
  base: 'users',
  orderby,
  search,
});
```

In order to use it just substitute the existing `getUrl` and `getODataUrl` methods. `getMockUrl` supports the same options in the same format with the exception of the `filter` property.\
If you choose a different `port` and/or `pageSize` you'll have to supply those options to `getMockUrl` when you invoke it.

```
const endpoint = getMockUrl({
  base: 'users',
  orderby,
  search,
  port: 5000,
  pageSize: 30,
});
```

## Running the repo

1. Run `npm i`
2. Create a schema folder with some schemas to generate the database.
3. Create a `json-server.json` file in the root of the project and point to the schema folder.
4. Run `npm start`

## Building

Building automatically occurs when `npm publish` is run, but you can also build it manually through `npm run build`
