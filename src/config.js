let config = {
  delay: 0,
  port: 4000,
  pageSize: 20,
  items: 'items',
  count: 'count',
  nextPageLink: 'next_page_link',
};

const getConfig = () => config;

const setConfig = (options) => {
  config = { ...config, ...options };
};

export { setConfig, getConfig };
