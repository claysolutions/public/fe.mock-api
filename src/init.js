import generateDb from './generateDb';
import serve from './server';
import loadSettings from './settings';

loadSettings()
  .then(generateDb)
  .then(serve)
  .catch((err) => console.log(err));
