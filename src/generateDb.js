import fs from 'mz/fs';
import path from 'path';
import faker from 'faker';
import { extend, resolve } from 'json-schema-faker';

import { getConfig } from './config';

const getSchemas = async (schemaPath, mainSchema) => {
  const promises = [];
  const schema = await fs.readFile(path.join(schemaPath, mainSchema));
  const files = await fs.readdir(schemaPath);

  files
    .filter((file) => file !== mainSchema && file.endsWith('.json'))
    .forEach((file) => {
      const promise = fs.readFile(path.join(schemaPath, file));
      promises.push(promise);
    });

  const refs = await Promise.all(promises);

  return {
    schema: JSON.parse(schema),
    refs: refs.map((ref) => JSON.parse(ref)),
  };
};

const generateDb = async () => {
  const { schemaPath, mainSchema, dbPath } = getConfig();

  if (!schemaPath || !mainSchema) {
    console.log('No schema path supplied');
    return null;
  }

  const { schema, refs } = await getSchemas(schemaPath, mainSchema);

  extend('faker', () => faker);

  const db = await resolve(schema, refs, path.resolve(schemaPath));

  if (dbPath) {
    fs.writeFile(path.resolve(dbPath, 'db.json'), JSON.stringify(db));
  }

  console.log('Database created successfully');
  return db;
};

export default generateDb;
