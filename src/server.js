import jsonServer from 'json-server';
import parse from 'parse-link-header';

import { getConfig } from './config';

const serve = (db) => {
  if (!db) {
    return;
  }

  const { port, items, count, nextPageLink, delay } = getConfig();
  const errorRegex = /\b(4|5)\d\d$/;

  const server = jsonServer.create();
  const router = jsonServer.router(db);
  const middlewares = jsonServer.defaults();

  server.use((req, res, next) => {
    setTimeout(next, delay);
  });

  server.use(middlewares);

  server.get(errorRegex, (req, res) => {
    const code = req.path.substring(1);

    res.status(code).send(db[code] ?? {});
  });

  router.render = (req, res) => {
    const { searchParams } = new URL(req.get('Host') + req.originalUrl);
    const error = searchParams.get('_error');

    if (errorRegex.test(error)) {
      return res.status(error).send(db[error] ?? {});
    }

    if (!searchParams.get('_limit')) {
      return res.json(res.locals.data);
    }

    res.json({
      [items]: res.locals.data,
      [count]: res.get('X-Total-Count'),
      [nextPageLink]: parse(res.get('Link'))?.next?.url ?? null,
    });
  };

  server.use(router);

  server.listen(port, () => {
    let message = `JSON Server is running at http://localhost:${port}`;

    if (delay) {
      message += ` with a delay of ${delay}ms`;
    }

    console.log(message);
  });
};

export default serve;
