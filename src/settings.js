import fs from 'mz/fs';
import { setConfig } from './config';

const loadSettings = async () => {
  try {
    const config = await fs.readFile('json-server.json');

    setConfig(JSON.parse(config));
  } catch (err) {
    if (err.code === 'ENOENT') {
      console.log('No json-server.json found at root');
    } else {
      console.log(err);
    }
  }
};

export default loadSettings;
