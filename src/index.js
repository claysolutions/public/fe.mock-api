const getBase = (url, port) => {
  const base = `http://localhost:${port}`;

  return new URL(url, base).href;
};

const getMockUrl = (options, port = 4000) => {
  if (typeof options === 'string') {
    return getBase(options, port);
  }

  const { pageSize = 20 } = options;

  const { skip = 0, top = pageSize, search, orderby, base } = options;
  const url = getBase(base, options.port ?? port);
  let queryString = `?_page=${skip / pageSize + 1}&_limit=${top}`;

  if (search && search.query) {
    const { keys, query } = search;

    if (Array.isArray(keys)) {
      queryString += `&q=${query}`;
    } else {
      queryString += `&${keys}_like=${query}`;
    }
  }

  if (orderby) {
    const [sort, order] = orderby.split(' ');
    queryString += `&_sort=${sort}&_order=${order}`;
  }

  return url + queryString;
};

// eslint-disable-next-line import/prefer-default-export
export { getMockUrl };
